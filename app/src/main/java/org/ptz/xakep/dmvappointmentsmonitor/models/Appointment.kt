package org.ptz.xakep.dmvappointmentsmonitor.models

data class Appointment(
    val officeName: String,
    val date: String,
    val purpose: String)