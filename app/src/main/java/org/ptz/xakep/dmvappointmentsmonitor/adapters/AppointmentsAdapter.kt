package org.ptz.xakep.dmvappointmentsmonitor.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.ptz.xakep.dmvappointmentsmonitor.R
import org.ptz.xakep.dmvappointmentsmonitor.models.Appointment
import kotlinx.android.synthetic.main.appointment_card.view.*
import kotlinx.android.synthetic.main.available_date_card.view.officeName

class AppointmentsAdapter : RecyclerView.Adapter<AppointmentsAdapter.ViewHolder>() {
    private var items: MutableList<Appointment> = mutableListOf()
    private lateinit var cancelBtnOnClickListener: View.OnClickListener

    fun setOnClickListener(listener: View.OnClickListener) {
        cancelBtnOnClickListener = listener
    }

    fun addData(item: Appointment) {
        items.add(item)
        notifyDataSetChanged()
    }

    fun addData(appointments: List<Appointment>) {
        items.addAll(appointments)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.appointment_card, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        holder.officeNameTxtView.text = data.officeName
        holder.availableDate.text = data.date
        holder.purpose.text = data.purpose

        holder.cancelAppointmentBtn.tag = position
        holder.cancelAppointmentBtn.setOnClickListener(cancelBtnOnClickListener)

        holder.cancelAppointmentBtn.visibility =
            if (data.purpose.isNotEmpty()) VISIBLE else GONE
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val officeNameTxtView: TextView = view.officeName
        val availableDate: TextView = view.date
        val purpose: TextView = view.purpose
        val cancelAppointmentBtn: Button = view.cancelAppointmentBtn
    }
}