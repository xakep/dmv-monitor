package org.ptz.xakep.dmvappointmentsmonitor.services

import android.content.Context
import org.ptz.xakep.dmvappointmentsmonitor.R
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.http.*

class RetrofitNetworkService @Inject constructor(
    private val ctx: Context) {
    fun getInstance(): IRetrofitNetworkService {
        return Retrofit.Builder()
            .baseUrl(ctx.getString(R.string.dmvHost))
            .addConverterFactory(CustomConverterFactory())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(IRetrofitNetworkService::class.java)
    }
}

interface IRetrofitNetworkService {

    // Get sessionId
    @HEAD("/wasapp/foa/officeVisit.do")
    fun getSessionIdAsync(): Deferred<Response<Void>>

    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/officeVisit.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/findOfficeVisit.do")
    fun getDatesForOfficeVisit(
        @Header("Cookie") cookie: String,
        @Field("mode") mode: String,
        @Field("captchaResponse") captchaResponse: String,
        @Field("officeId") officeId: String,
        @Field("numberItems") numberItems: Int,
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("telArea") telArea: String,
        @Field("telPrefix") telPrefix: String,
        @Field("telSuffix") telSuffix: String,
        @Field("resetCheckFields") resetCheckFields: String,
        @Field("g-recaptcha-response") grecaptcharesponse: String,
        @Field("taskRID") taskRID: Boolean,
        @Field("taskCID") taskCID: Boolean,
        @Field("taskVR") taskVR: Boolean,
        @Field("_taskRID") _taskRID: String = "on", // real id
        @Field("_taskCID") _taskCID: String = "on", // california id
        @Field("_taskVR") _taskVR: String = "on" // vehicle registration
    ): Deferred<Response<String>>

    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/findOfficeVisit.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/findDriveTest.do")
    fun getDatesForDrivingTest(
        @Header("Cookie") cookie: String,
        @Field("numberItems") numberItems: Int,
        @Field("mode") mode: String,
        @Field("captchaResponse") captchaResponse: String,
        @Field("officeId") officeId: String,
        @Field("requestedTask") requestedTask: String,
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("dlNumber") dlNumber: String,
        @Field("birthMonth") birthMonth: String,
        @Field("birthDay") birthDay: String,
        @Field("birthYear") birthYear: String,
        @Field("telArea") telArea: String,
        @Field("telPrefix") telPrefix: String,
        @Field("telSuffix") telSuffix: String,
        @Field("resetCheckFields") resetCheckFields: String,
        @Field("g-recaptcha-response") grecaptcharesponse: String
    ): Deferred<Response<String>>

    // used by settings activity
    @GET("/wasapp/foa/clear.do?goTo=driveTest")
    fun getOfficesAndDataKey(): Deferred<Response<String>>

    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/searchAppts.do")
    fun getMyAppointments(
        @Header("Cookie") cookie: String,
        @Field("captchaResponse") captchaResponse: String,
        @Field("firstName") firstName: String,
        @Field("lastName") lastName: String,
        @Field("telArea") telArea: String,
        @Field("telPrefix") telPrefix: String,
        @Field("telSuffix") telSuffix: String,
        @Field("g-recaptcha-response") grecaptcharesponse: String
    ): Deferred<Response<String>>

    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/routeCancelApptRequest.do")
    fun cancelAppointment(
        @Header("Cookie") cookie: String,
        @Field("chosenApptIndex") chosenApptIndex: Int
    ): Deferred<Response<String>>

    /*
    requests to make an appointment
     */

    // first one
    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/checkForOfficeVisitConflicts.do")
    fun checkForConflicts(
        @Header("Cookie") cookie: String,
        @Field("chosenOfficeId") chosenOfficeId: Int
    ): Deferred<Response<String>>

    // second one
    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @FormUrlEncoded
    @POST("/wasapp/foa/selectNotification.do")
    fun selectNotification(
        @Header("Cookie") cookie: String,
        @Field("telArea") telArea: String,
        @Field("telPrefix") telPrefix: String,
        @Field("telSuffix") telSuffix: String,
        // all other fields are empty by default
        @Field("notify_smsTelArea") smsTelArea: String = "",
        @Field("notify_smsTelPrefix") smsTelPrefix: String = "",
        @Field("notify_smsTelSuffix") smsTelSuffix: String = "",
        @Field("notify_smsTelArea_confirm") smsTelAreaConfirm: String = "",
        @Field("notify_smsTelPrefix_confirm") smsTelPrefixConfirm: String = "",
        @Field("notify_smsTelSuffix_confirm") smsTelSuffixConfirm: String = "",
        @Field("notify_email") notifyEmail: String = "",
        @Field("notify_email_confirm") notifyEmailConfirm: String = "",
        @Field("notify_telArea") notifyTelArea: String = "",
        @Field("notify_telPrefix") notifyTelPrefix: String = "",
        @Field("notify_telSuffix") notifyTelSuffix: String = "",
        @Field("notificationMethod") notificationMethod: String = "NONE"
    ): Deferred<Response<String>>

    // last one
    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @POST("/wasapp/foa/confirmAppt.do")
    fun confirmAppointment(
        @Header("Cookie") cookie: String
    ): Deferred<Response<String>>

    // this gets called while booking process
    // in case there is existing appointment which is going to be overridden
    @Headers(
        "Origin: https://www.dmv.ca.gov",
        "Upgrade-Insecure-Requests: 1",
        "Content-Type: application/x-www-form-urlencoded",
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36",
        "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Referer: https://www.dmv.ca.gov/wasapp/foa/viewCancel.do",
        "Accept-Language: en-US,en;q=0.9,ru;q=0.8",
        "Connection: keep-alive",
        "Cache-Control: max-age=0",
        "Host: www.dmv.ca.gov",
        "Accept-Encoding: deflate"
    )
    @POST("/wasapp/foa/cancelExistingAppt.do")
    fun cancelExistingAppointment(
        @Header("Cookie") cookie: String
    ): Deferred<Response<String>>
}