package org.ptz.xakep.dmvappointmentsmonitor.modules

import android.content.Context
import org.ptz.xakep.dmvappointmentsmonitor.services.PreferencesService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ServicesModule {
    @Provides
    @Singleton
    fun providePrefsService(ctx: Context): PreferencesService = PreferencesService(ctx)
}