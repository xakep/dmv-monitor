package org.ptz.xakep.dmvappointmentsmonitor.modules

import android.content.Context
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {
    @Provides
    fun provideRetrofit(ctx: Context) : RetrofitNetworkService = RetrofitNetworkService(ctx)
}