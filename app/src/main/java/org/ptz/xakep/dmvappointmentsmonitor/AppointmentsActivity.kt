package org.ptz.xakep.dmvappointmentsmonitor

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.ptz.xakep.dmvappointmentsmonitor.adapters.AppointmentsAdapter
import org.ptz.xakep.dmvappointmentsmonitor.models.Appointment
import org.ptz.xakep.dmvappointmentsmonitor.services.PreferencesService
import org.ptz.xakep.dmvappointmentsmonitor.services.RECAPTCHA_REQ
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import org.ptz.xakep.dmvappointmentsmonitor.services.Utils
import org.ptz.xakep.dmvappointmentsmonitor.viewModels.AppointmentsViewModel
import kotlinx.android.synthetic.main.activity_monitoring.*
import javax.inject.Inject

class AppointmentsActivity : AppCompatActivity() {

    private lateinit var adapter: AppointmentsAdapter
    private lateinit var viewModel: AppointmentsViewModel
    private lateinit var appointments: List<Appointment>

    @Inject
    lateinit var prefsService: PreferencesService

    @Inject
    lateinit var networkService: RetrofitNetworkService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointments)

        val component = (this.application as DmvMonitorApplication).appointmentsComponent
        component.inject(this)

        val viewManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        datesList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }

        viewModel = ViewModelProviders
            .of(this, AppointmentsViewModel.FACTORY(networkService))
            .get(AppointmentsViewModel::class.java)

        adapter = AppointmentsAdapter()
        adapter.setOnClickListener(cancelBtnListener)
        datesList.adapter = adapter

        if (intent.getBooleanExtra(getString(R.string.loadAppointmentsNow), true)) {
            load()
        }

        refreshNowBtn.setOnClickListener{
            load()
        }

        viewModel.appointments.observe(this, Observer { value ->
            value?.let {
                loadFinished(it)
            }
        })

        viewModel.errors.observe(this, Observer { value ->
            value?.let {
                onError()
            }
        })
    }

    private val cancelBtnListener = View.OnClickListener {
        Utils.showConfirmationDialog(this) {
            showContent(false)

            val position = (it as Button).tag as Int

            val code = prefsService.get(application.getString(R.string.recaptchaResponseCode))
            val firstName = prefsService.get(application.getString(R.string.firstName))
            val lastName = prefsService.get(application.getString(R.string.lastName))
            val phone = prefsService.get(application.getString(R.string.phone)).split(".")

            viewModel.cancel(code, firstName, lastName, phone, appointments[position], position)
        }
    }

    private fun load() {
        if (prefsService.get(getString(R.string.recaptchaResponseCode)) == "") {
            solveRecaptcha()
        } else {
            showContent(false)

            val code = prefsService.get(application.getString(R.string.recaptchaResponseCode))
            val firstName = prefsService.get(application.getString(R.string.firstName))
            val lastName = prefsService.get(application.getString(R.string.lastName))
            val phone = prefsService.get(application.getString(R.string.phone)).split(".")

            viewModel.refresh(
                code, firstName, lastName, phone
            )
        }
    }

    private fun loadFinished(data: List<Appointment>) {
        adapter.clear()
        if (data.any()) {
            appointments = data
            adapter.addData(data)

        } else {
            adapter.addData(Appointment(getString(R.string.noAppointmentsFound), "", ""))
        }

        showContent(true)
    }

    private fun onError() {
        adapter.addData(Appointment(getString(R.string.noConnectionError), "", ""))
        showContent(true)
    }

    private fun showContent(show: Boolean) = if (!show) {
        // loading
        adapter.clear()
        refreshNowBtn.isEnabled = false
        datesList.visibility = View.INVISIBLE
        progressBar.visibility = View.VISIBLE
    } else {
        // loaded
        refreshNowBtn.isEnabled = true
        progressBar.visibility = View.INVISIBLE
        datesList.visibility = View.VISIBLE
    }

    private fun solveRecaptcha() {
        val intent = Intent(applicationContext, RecaptchaActivity::class.java)
        intent.putExtra(
            getString(R.string.dataSiteKey),
            prefsService.get(getString(R.string.dataSiteKey))
        )
        startActivityForResult(intent, RECAPTCHA_REQ)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val code = data?.getStringExtra(getString(R.string.recaptchaCode)) ?: ""
            prefsService.save(getString(R.string.recaptchaResponseCode), code)
        }
    }
}
