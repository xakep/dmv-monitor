package org.ptz.xakep.dmvappointmentsmonitor.models

data class Office(var name: String, var id: Int, var selected: Boolean)