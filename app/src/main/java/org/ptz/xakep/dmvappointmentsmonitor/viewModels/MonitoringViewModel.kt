package org.ptz.xakep.dmvappointmentsmonitor.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.ptz.xakep.dmvappointmentsmonitor.models.ExistingAppointment
import org.ptz.xakep.dmvappointmentsmonitor.models.NotificationWrapper
import org.ptz.xakep.dmvappointmentsmonitor.parsers.AppointmentsPageParser
import org.ptz.xakep.dmvappointmentsmonitor.services.IRetrofitNetworkService
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MonitoringViewModel(private val network: RetrofitNetworkService): ViewModel() {
    companion object {
        val FACTORY = singleArgViewModelFactory(::MonitoringViewModel)
    }

    private val _bookingFinished = MutableLiveData<Boolean>()

    val bookingFinished: LiveData<Boolean>
        get() = _bookingFinished

    private val _conflictAppointment = MutableLiveData<ExistingAppointment>()

    val conflictAppointment: LiveData<ExistingAppointment>
        get() = _conflictAppointment

    // used when service gets stopped after booking
    private val _serviceStopped = MutableLiveData<Boolean>()

    val serviceStopped: LiveData<Boolean>
        get() = _serviceStopped

    // make an appointment
    // if askUserConfirmation = false, send a notification instead
    fun book(
        sessionId: String,
        position: Int, // position of available date on the page
        phone: List<String>,
        askUserConfirmation: Boolean = true, // should I ask the User in case of conflicts?
        notification: NotificationWrapper? = null
    ) {
        val service = network.getInstance()
        viewModelScope.launch {
            try {
                val r1 = service.checkForConflicts(sessionId, position).await()
                if (!r1.isSuccessful) {
                    notifyActivity(askUserConfirmation, false)
                    return@launch
                } else {
                    // check the response for conflicts
                    val conflicts = AppointmentsPageParser()
                        .parseForConflicts(r1.body() ?: "")
                    if (conflicts.any()) {
                        if (askUserConfirmation) {
                            conflicts[1].sessionId = sessionId
                            _conflictAppointment.value = conflicts[1]

                            return@launch
                        } else {
                            // proceed w/o user consent and cancel existing appointment
                            service.cancelExistingAppointment(sessionId).await()
                        }
                    } else {
                        delay(200) // no conflicts found, proceed after delay
                    }
                }

                val res = finishBooking(service, sessionId, phone)
                notifyActivity(askUserConfirmation, res)

                if (!askUserConfirmation && res) {
                    // send a notification
                    if (notification?.sendNotification() == true) {
                        // update button text
                        _serviceStopped.value = true
                    }
                }
            } catch (e: Exception) {
                // do nothing...
                notifyActivity(askUserConfirmation, false)
            }
        }
    }

    fun continueAfterConflict(
        sessionId: String,
        phone: List<String>
    ) {
        val service = network.getInstance()
        viewModelScope.launch {
            try {
                // cancel existing one
                service.cancelExistingAppointment(sessionId).await()

                // finish
                val res = finishBooking(service, sessionId, phone)
                notifyActivity(true, res)
            } catch (e: Exception) {
                // do nothing...
                notifyActivity(true, false)
            }
        }
    }

    private suspend fun finishBooking(
        service: IRetrofitNetworkService,
        sessionId: String,
        phone: List<String>): Boolean {
        val r2 = service.selectNotification(sessionId, phone[0], phone[1], phone[2]).await()
        if (!r2.isSuccessful) {
            return false
        }
        delay(200)

        val r3 = service.confirmAppointment(sessionId).await()
        if (!r3.isSuccessful) {
            return false
        }

        return true
    }

    // we don't need to notify activity if called from the service (notify = false)
    private fun notifyActivity(notify: Boolean, result: Boolean) {
        if (notify) {
            _bookingFinished.value = result
        }
    }
}