package org.ptz.xakep.dmvappointmentsmonitor.services

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import org.ptz.xakep.dmvappointmentsmonitor.R
import javax.inject.Inject

class PreferencesService @Inject constructor(private var ctx: Context) {

    private val prefsName = "prefs"

    fun save(key: String, value: String) {
        val prefs = getPrefs()
        val ed = prefs.edit()
        ed.putString(key, value)
        ed.apply()
    }

    fun get(key: String): String {
        val prefs = getPrefs()
        return prefs.getString(key, "")
    }

    fun isConfiguredForDriveTest(): Boolean {
        if (!isConfiguredForOfficeVisit()) {
            return false
        }

        val prefs = getPrefs()

        if (prefs.getString(ctx.getString(R.string.dlNumber), "") == "") {
            return false
        }

        if (prefs.getString(ctx.getString(R.string.birthDate), "") == "") {
            return false
        }

        return true
    }

    fun isConfiguredForOfficeVisit(): Boolean {
        val prefs = getPrefs()
        if (prefs.getString(ctx.getString(R.string.firstName), "") == "") {
            return false
        }
        if (prefs.getString(ctx.getString(R.string.lastName), "") == "") {
            return false
        }

        if (prefs.getString(ctx.getString(R.string.phone), "") == "") {
            return false
        }
        if (prefs.getString(ctx.getString(R.string.dataSiteKey), "") == "") {
            return false
        }

        return true
    }

    private fun getPrefs(): SharedPreferences {
        return ctx.getSharedPreferences(prefsName, MODE_PRIVATE)
    }
}