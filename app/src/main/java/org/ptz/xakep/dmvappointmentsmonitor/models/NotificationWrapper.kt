package org.ptz.xakep.dmvappointmentsmonitor.models

import android.app.Notification

data class NotificationWrapper(
    private val notificationId: Int,
    private val notification: Notification,
    private val sendNotificationFunc: (id: Int, notification: Notification) -> Boolean) {
    fun sendNotification(): Boolean {
        return sendNotificationFunc(notificationId, notification)
    }
}