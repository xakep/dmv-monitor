package org.ptz.xakep.dmvappointmentsmonitor

import android.content.DialogInterface.OnMultiChoiceClickListener
import android.content.Intent
import android.os.Bundle
import android.view.View.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.ptz.xakep.dmvappointmentsmonitor.models.Office
import org.ptz.xakep.dmvappointmentsmonitor.models.SettingsActivityMode
import org.ptz.xakep.dmvappointmentsmonitor.services.MAX_OFFICES_TO_MONITOR
import org.ptz.xakep.dmvappointmentsmonitor.services.MESSENGER_REQ_MODES
import org.ptz.xakep.dmvappointmentsmonitor.services.PreferencesService
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import org.ptz.xakep.dmvappointmentsmonitor.viewModels.SettingsViewModel
import kotlinx.android.synthetic.main.activity_settings.*
import java.sql.Date
import javax.inject.Inject

class SettingsActivity : AppCompatActivity() {

    private lateinit var viewModel: SettingsViewModel
    private var isSaved = false

    private fun onError() {
        proceedBtn.isEnabled = true
        progressBar.visibility = INVISIBLE
        errorTextView.visibility = VISIBLE
        proceedBtn.text = getString(R.string.tryagain)
        proceedBtn.setOnClickListener {
            fetchOffices()
        }
    }

    private fun fetchOffices() {
        proceedBtn.isEnabled = false
        errorTextView.visibility = GONE
        proceedBtn.text = getString(R.string.proceedBtn)
        showContent(false)

        viewModel.refresh()
    }

    private fun showContent(show: Boolean) {
        if (show) {
            setOnClickListener()
            proceedBtn.isEnabled = true
            errorTextView.visibility = GONE
            proceedBtn.text = getString(R.string.proceedBtn)

            progressBar.visibility = GONE
            openDialogBtn.visibility = VISIBLE
        } else {
            openDialogBtn.visibility = GONE
            progressBar.visibility = VISIBLE
        }
    }

    private fun setOnClickListener() {
        proceedBtn.setOnClickListener {
            val mode = intent.getSerializableExtra(getString(R.string.settingsActivityMode)) as SettingsActivityMode // -1 means don't proceed
            val errors = save(mode)
            if (mode == SettingsActivityMode.NONE) {
                finish() // go back to main activity
            } else {
                if (errors != "") {
                    Toast.makeText(this, errors, Toast.LENGTH_LONG).show()
                } else {
                    isSaved = true

                    if (mode != SettingsActivityMode.APPTS) { // rid or cid
                        val newIntent = Intent(applicationContext, MonitoringActivity::class.java)

                        if (mode == SettingsActivityMode.RID) {
                            newIntent.putExtra(
                                MESSENGER_REQ_MODES, // RID modes
                                intent.getBooleanArrayExtra(MESSENGER_REQ_MODES)
                                    ?: booleanArrayOf(false, true, false)
                            )
                        }

                        startActivity(newIntent)
                    } else { // appointments then
                        val newIntent = Intent(applicationContext, AppointmentsActivity::class.java)
                        newIntent.putExtra(
                            getString(R.string.loadAppointmentsNow),
                            false
                        ) // no recapctcha yet

                        startActivity(newIntent)
                    }
                }
            }
        }
    }

    @Inject
    lateinit var prefsService: PreferencesService

    @Inject
    lateinit var networkService: RetrofitNetworkService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        setOnClickListener()

        val component = (this.application as DmvMonitorApplication).settingsComponent
        component.inject(this)

        viewModel = ViewModelProviders
            .of(this, SettingsViewModel.FACTORY(networkService))
            .get(SettingsViewModel::class.java)
        fetchOffices()

        openDialogBtn.setOnClickListener {
            val adb = AlertDialog.Builder(this)
            adb.setTitle(R.string.selectOfficesDialogTitle)
            adb.setMultiChoiceItems(
                offices.map(Office::name).toTypedArray(),
                offices.map(Office::selected).toBooleanArray(),
                myItemsMultiClickListener
            )
            adb.setPositiveButton(R.string.okBtn, null)
            adb.setCancelable(false)
            adb.show()
        }

        viewModel.dataSiteKey.observe(this, Observer { value ->
            value?.let {
                prefsService.save(getString(R.string.dataSiteKey), it)
            }
        })

        viewModel.offices.observe(this, Observer { value ->
            value?.let {
                if (!it.any()) {
                    onError()
                } else {
                    val selectedOffices = prefsService.get(getString(R.string.selectedOffices))
                    if (selectedOffices != "") {
                        val a = selectedOffices.split(",")
                        for (office in it) {
                            if (a.contains(office.id.toString())) {
                                office.selected = true
                            }
                        }
                    }

                    offices = it
                    showContent(true)
                }
            }
        })
    }

    private var offices: ArrayList<Office> = arrayListOf()

    private var myItemsMultiClickListener: OnMultiChoiceClickListener =
        OnMultiChoiceClickListener { _, which, isChecked ->
            offices[which].selected = isChecked
            val count = getSelectedCount()
            if (count > MAX_OFFICES_TO_MONITOR) {
                val msg = String.format(getString(R.string.officesLimitMsg), count)
                Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show()
            }
        }

    override fun onStart() {
        super.onStart()

        var name = prefsService.get(getString(R.string.firstName))
        if (name.isEmpty()) name = "taylor"
        firstNameEditText.setText(name)
        var last = prefsService.get(getString(R.string.lastName))
        if (last.isEmpty()) last = "jenkins"
        lastNameEditText.setText(last)
        dlNumberEditText.setText(prefsService.get(getString(R.string.dlNumber)))
        birthdateEditText.setText(prefsService.get(getString(R.string.birthDate)))
        var phone = prefsService.get(getString(R.string.phone))
        if (phone.isEmpty()) phone = "323.217.9744"
        phoneEditText.setText(phone)

        val interval = prefsService.get(getString(R.string.checkInterval))
        monitorInterval.setText(if (interval == "") "30" else interval)

        val daysInterval = prefsService.get(getString(R.string.daysInterval))
        notificationsInterval.setText(if (daysInterval == "") "14" else daysInterval)
    }

    override fun onStop() {
        super.onStop()
        if (!isSaved) save() else isSaved = false
    }

    private fun save(mode: SettingsActivityMode = SettingsActivityMode.NONE): String {
        val errors = mutableListOf<String>()

        val interval = monitorInterval.text.toString()
        if (interval == "") {
            errors.add("Checking interval must not be empty (min 15)")
        } else {
            if (interval.toInt() < 15) {
                errors.add("Checking interval cannot be less than 15")
            } else {
                prefsService.save(getString(R.string.checkInterval), interval)
            }
        }

        val notifInterval = notificationsInterval.text.toString()
        if (notifInterval == "") {
            errors.add("Days count must not be empty")
        } else {
            if (notifInterval.toInt() < 1) {
                errors.add("Days count cannot be less than 1")
            } else {
                prefsService.save(getString(R.string.daysInterval), notifInterval)
            }
        }

        val firstName = firstNameEditText.text.toString()
        if (firstName == "") {
            errors.add("First Name must not be empty")
        } else {
            prefsService.save(getString(R.string.firstName), firstName)
        }

        val lastName = lastNameEditText.text.toString()
        if (lastName == "") {
            errors.add("Last Name must not be empty")
        } else {
            prefsService.save(getString(R.string.lastName), lastName)
        }

        val phone = phoneEditText.text.toString()
        if (!isPhoneValid(phone)) {
            errors.add("Phone is invalid, should be ###.###.####")
        } else {
            prefsService.save(getString(R.string.phone), phone)
        }

        val selected = getSelectedOffices().replace(" ", "")
        if (selected == "") {
            errors.add("Select at least one office")
        } else {
            prefsService.save(getString(R.string.selectedOffices), selected)
        }

        if (mode == SettingsActivityMode.CID) {
            val dlNumber = dlNumberEditText.text.toString()
            if (dlNumber == "") {
                errors.add("Driver License number must not be empty")
            } else {
                prefsService.save(getString(R.string.dlNumber), dlNumber)
            }

            val dob = birthdateEditText.text.toString()
            if (!isDoBValid(dob)) {
                errors.add("Birthdate is invalid, should be mm/dd/yyyy")
            } else {
                prefsService.save(getString(R.string.birthDate), dob)
            }
        }

        if (errors.isNotEmpty()) {
            return errors.joinToString(separator = "\n")
        }

        return ""
    }

    private fun isDoBValid(dob: String): Boolean {
        if (dob.indexOf("/") == -1) return false
        val parts = dob.split("/")
        if (parts.size != 3 || parts[0].length != 2 || parts[1].length != 2 || parts[2].length != 4) return false
        try {
            Date.valueOf(parts[2] + "-" + parts[0] + "-" + parts[1])
        } catch (e: IllegalArgumentException) {
            return false
        }

        return true
    }

    private fun isPhoneValid(phone: String): Boolean {
        if (phone.indexOf(".") == -1) return false
        val parts = phone.split(".")
        if (parts.size != 3 || parts[0].length != 3 || parts[1].length != 3 || parts[2].length != 4) return false
        try {
            parts[0].toInt()
            parts[1].toInt()
            parts[2].toInt()
        } catch (e: NumberFormatException) {
            return false
        }

        return true
    }

    private fun getSelectedCount(): Int {
        if (offices.isEmpty()) return 0
        return offices
            .filter { it.selected }
            .count()
    }

    private fun getSelectedOffices(): String {
        if (offices.isEmpty()) return ""
        return offices
            .filter { it.selected }
            .map { it.id }
            .joinToString()
    }
}
