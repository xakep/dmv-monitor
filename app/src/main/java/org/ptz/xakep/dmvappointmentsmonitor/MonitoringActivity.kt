package org.ptz.xakep.dmvappointmentsmonitor

import android.app.Activity
import android.app.AlertDialog
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Messenger
import android.os.PersistableBundle
import android.view.View.*
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import org.ptz.xakep.dmvappointmentsmonitor.adapters.AvailableDatesAdapter
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.services.*
import org.ptz.xakep.dmvappointmentsmonitor.viewModels.MonitoringViewModel
import kotlinx.android.synthetic.main.activity_monitoring.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MonitoringActivity : AppCompatActivity(), IMonitoringView {
    private lateinit var viewModel: MonitoringViewModel

    override fun onError(code: Int) {
        showContent(true)
        when (code) {
            SERVICE_FAILURE -> {
                Toast.makeText(this, getString(R.string.noConnectionError), Toast.LENGTH_LONG).show()
            }
            WRONG_PERSONAL_INFO -> {
                serviceStopped()
                Toast.makeText(this, getString(R.string.wrongPIerror), Toast.LENGTH_LONG).show()
            }
            EXPIRED_RECAPTCHA -> {
                prefsService.save(getString(R.string.recaptchaResponseCode), "")
                solveRecaptcha()
            }
        }
    }

    override fun loadFinished(data: List<AvailableDate>) {
        adapter.clear() // TODO: move to adapter
        adapter.addData(data)
        showContent(true)
    }

    override fun serviceStopped() {
        isStarted = false
        startMonitoringBtn.text = getString(R.string.startMonitoringBtn)
    }

    private val makeAppointmentBtnListener = OnClickListener {
        Utils.showConfirmationDialog(this) {
            val position = (it as Button).tag as Int // position in sorted list
            val item = adapter.getItemByPosition(position)
            if (item != null) {
                showContent(false)

                viewModel.book(
                    item.sessionId,
                    item.position, // position in the response
                    prefsService.get(application.getString(R.string.phone)).split(".")
                )
            }
        }
    }

    private fun showContent(show: Boolean) = if (!show) {
        refreshNowBtn.isEnabled = false
        datesList.visibility = INVISIBLE
        progressBar.visibility = VISIBLE
    } else {
        refreshNowBtn.isEnabled = true
        progressBar.visibility = INVISIBLE
        datesList.visibility = VISIBLE
    }

    @Inject
    lateinit var prefsService: PreferencesService

    @Inject
    lateinit var networkService: RetrofitNetworkService

    private lateinit var handler: JobMessagesHandler
    private lateinit var adapter: AvailableDatesAdapter
    private lateinit var serviceComponent: ComponentName
    private var isStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monitoring)

        val viewManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        datesList.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
        }

        handler = JobMessagesHandler(this)
        val requestedModes = intent.getBooleanArrayExtra(MESSENGER_REQ_MODES)
        serviceComponent = if (requestedModes == null)
            ComponentName(this, DrivingTestMonitoringService::class.java) else
            ComponentName(this, OfficeVisitMonitoringService::class.java)

        val component = (this.application as DmvMonitorApplication).monitoringComponent
        component.inject(this)

        viewModel = ViewModelProviders
            .of(this, MonitoringViewModel.FACTORY(networkService))
            .get(MonitoringViewModel::class.java)

        refreshNowBtn.setOnClickListener {
            adapter.clear()
            if (prefsService.get(getString(R.string.recaptchaResponseCode)) == "") {
                solveRecaptcha()
            } else {
                showContent(false)
                initService(true)
            }
        }

        startMonitoringBtn.setOnClickListener {
            if (isStarted) {
                stopService()
            } else {
                if (prefsService.get(getString(R.string.recaptchaResponseCode)) == "") {
                    solveRecaptcha()
                } else {
                    showContent(false)
                    scheduleJob()
                }
            }
        }

        adapter = AvailableDatesAdapter()
        adapter.setOnClickListener(makeAppointmentBtnListener)
        datesList.adapter = adapter

        viewModel.bookingFinished.observe(this, Observer { value ->
            showContent(true)
            val msg = if (value)
                getString(R.string.bookingSuccessful) else
                getString(R.string.bookingUnsuccessful)
            Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show()
        })

        viewModel.conflictAppointment.observe(this, Observer { existingAppointment ->
            showContent(true)

            val alert = AlertDialog.Builder(this)
            val message = String.format(
                getString(R.string.appointmentConflictsDialogTitle),
                existingAppointment.officeName + " " + existingAppointment.date)
            alert.setMessage(message)

            alert.setPositiveButton(getString(R.string.keepItBtn)) { dialog, _ ->
                dialog.dismiss() // do nothing
            }

            alert.setNegativeButton(getString(R.string.confirmNewApptDialogBtn)) { dialog, _ ->
                dialog.dismiss()
                showContent(false)
                viewModel.continueAfterConflict(
                    existingAppointment.sessionId,
                    prefsService.get(application.getString(R.string.phone)).split(".")
                )
            }

            alert.show()
        })

        viewModel.serviceStopped.observe(this, Observer { value ->
            if (value) {
                startMonitoringBtn.text = getString(R.string.startMonitoringBtn)
            }
        })
    }

    private fun solveRecaptcha() {
        val intent = Intent(applicationContext, RecaptchaActivity::class.java)
        intent.putExtra(
            getString(R.string.dataSiteKey),
            prefsService.get(getString(R.string.dataSiteKey))
        )
        startActivityForResult(intent, RECAPTCHA_REQ)
    }

    override fun onStart() {
        super.onStart()
        initService(false)
    }

    private fun initService(refresh: Boolean) {
        val requestedModes = intent.getBooleanArrayExtra(MESSENGER_REQ_MODES)
        val startServiceIntent = if (requestedModes == null)
            Intent(this, DrivingTestMonitoringService::class.java) else
            Intent(this, OfficeVisitMonitoringService::class.java)
        val messengerIncoming = Messenger(handler)
        startServiceIntent.putExtra(MESSENGER_INTENT_KEY, messengerIncoming)
        startServiceIntent.putExtra(
            MESSENGER_REQ_MODES,
            intent.getBooleanArrayExtra(MESSENGER_REQ_MODES) ?: booleanArrayOf(false, true, false))

        if (refresh)
            startServiceIntent.putExtra(MESSENGER_REFRESH_NOW, true)
        startService(startServiceIntent)

        if (isRunning()) {
            isStarted = true
            startMonitoringBtn.text = getString(R.string.stopMonitoringBtn)
        }
    }

    private fun isRunning(): Boolean {
        return getScheduler().allPendingJobs.size != 0
    }

    private fun stopService() {
        serviceStopped()

        val scheduler = getScheduler()
        scheduler.cancelAll()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val code = data?.getStringExtra(getString(R.string.recaptchaCode)) ?: ""
            prefsService.save(getString(R.string.recaptchaResponseCode), code)
        }
    }

    private fun getScheduler(): JobScheduler {
        return (getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler)
    }

    private fun scheduleJob() {
        isStarted = true
        startMonitoringBtn.text = getString(R.string.stopMonitoringBtn)
        val scheduler = getScheduler()
        scheduler.cancelAll()

        val builder = JobInfo.Builder(3119, serviceComponent)
        var checkInterval = prefsService.get(getString(R.string.checkInterval)).toLong()
        if (checkInterval < 15L) {
            checkInterval = 15L // minimum interval is 15 minutes
        }

        builder.setPeriodic(TimeUnit.MINUTES.toMillis(checkInterval))
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)

        val modes = intent.getBooleanArrayExtra(MESSENGER_REQ_MODES)
        // in case of RID propagate modes
        if (modes != null) {
            val extras = PersistableBundle()
            extras.putBooleanArray(MESSENGER_REQ_MODES, modes)
            builder.setExtras(extras)
        }

        scheduler.schedule(builder.build())
    }
}
