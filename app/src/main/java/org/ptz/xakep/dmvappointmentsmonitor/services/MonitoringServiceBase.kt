package org.ptz.xakep.dmvappointmentsmonitor.services

import android.app.Notification
import android.app.NotificationManager
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.app.job.JobService
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import androidx.core.app.NotificationCompat
import org.ptz.xakep.dmvappointmentsmonitor.R
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.models.NotificationWrapper
import org.ptz.xakep.dmvappointmentsmonitor.viewModels.MonitoringViewModel

abstract class MonitoringServiceBase : JobService() {

    protected var activityMessenger: Messenger? = null
    protected val data = mutableListOf<AvailableDate>()

    override fun onStopJob(params: JobParameters): Boolean {
        // Return false to drop the job.
        return false
    }

    protected suspend fun getSessionId(service: IRetrofitNetworkService): String {
        return try {
            return Utils.extractSessionId(service.getSessionIdAsync().await())
        } catch (e: Exception) {
            ""
        }
    }

    protected fun notifyActivity(
        messageID: Int,
        dates: List<AvailableDate>,
        prefsService: PreferencesService?
    ) {
        if (messageID == WRONG_PERSONAL_INFO) {
            cancelMonitoring()
        }

        val days = prefsService?.get(getString(R.string.daysInterval))?.toIntOrNull() ?: 14 // 2 weeks by default
        val sortedDates = dates.sortedBy { it.inDays }
        if (sortedDates.any { it.inDays <= days }) {
            makeAppointment(
                sortedDates[0],
                prefsService?.get(application.getString(R.string.phone))?.split(".") ?: listOf())
        }

        sendMsgToActivity(messageID, sortedDates)
    }

    protected fun filterOutDuplicateRecords(input: List<AvailableDate>): List<AvailableDate> {
        val results = mutableListOf<AvailableDate>()
        val groups = input.groupBy { it.officeName }
        for (name in groups.keys) {
            val group = groups.getValue(name)
            if (group.size > 1) {
                // false (main office) goes first
                val sortedByPriority = group.sortedBy { it.additionalOffice }
                results.add(sortedByPriority.first())
            } else {
                results.addAll(group) // group.size <= 1
            }
        }

        return results
    }

    private fun makeAppointment(
        date: AvailableDate,
        phone: List<String>) {
        try {
            if (phone.any()) {
                val viewModel = MonitoringViewModel(RetrofitNetworkService(application))

                val style = NotificationCompat.InboxStyle()
                style.addLine(date.officeName + ": " + date.date)
                val notification = NotificationCompat.Builder(this, NOTIF_CHANNEL)
                    .setSmallIcon(R.drawable.ic_notify)
                    .setContentTitle("You have a new appointment ${Utils.beautifyInDays(date.inDays)}")
                    .setStyle(style)
                    .build()

                // this is async call, returns immediately
                viewModel.book(
                    date.sessionId,
                    date.position,
                    phone,
                    askUserConfirmation = false,
                    notification = NotificationWrapper(NEW_APP_NOTIF, notification, ::sendNotification)
                ) // override existing appointment w/o user permission
            }
        } catch (e: Exception) {

        }
    }

    private fun sendNotification(notificationId: Int, notification: Notification): Boolean {
        return try {
            (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
                .notify(notificationId, notification)

            // cancel monitoring
            cancelMonitoring()

            sendMsgToActivity(BOOKING_COPMLETED) // update activity (start/stop button title)

            true
        } catch (e: Exception) {
            // do nothing
            false
        }
    }

    private fun cancelMonitoring() {
        (getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler).cancelAll()
    }

    private fun sendMsgToActivity(messageId: Int, data: Any? = null) {
        // If this service is launched by the JobScheduler, there's no callback Messenger. It
        // only exists when the MonitoringActivity calls startService() with the callback in the Intent.
        if (activityMessenger == null) {
            return
        }

        val message = Message.obtain()
        message.run {
            what = messageId
            obj = data
        }
        try {
            activityMessenger?.send(message)
        } catch (e: RemoteException) {
        }
    }
}