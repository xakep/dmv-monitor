package org.ptz.xakep.dmvappointmentsmonitor.services

import android.app.Service
import android.app.job.JobParameters
import android.content.Intent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.ptz.xakep.dmvappointmentsmonitor.R
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDates
import org.ptz.xakep.dmvappointmentsmonitor.parsers.AvailableDatesPageParser

class OfficeVisitMonitoringService : MonitoringServiceBase() {

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        activityMessenger = intent.getParcelableExtra(MESSENGER_INTENT_KEY)

        // get additional data from the intent (default is CID only)
        val officeVisitModes = intent.getBooleanArrayExtra(MESSENGER_REQ_MODES) ?: booleanArrayOf(false, true, false)
        if (intent.getBooleanExtra(MESSENGER_REFRESH_NOW, false)) {
            fetchData(null, officeVisitModes)
        } else if (data.size != 0) {
            notifyActivity(SUCCESS_MSG, data, null)
        }

        // service will NOT be restarted in case of system kills it
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters): Boolean {
        val officeVisitModes = params
            .extras.getBooleanArray(MESSENGER_REQ_MODES) ?: booleanArrayOf(false, true, false)
        fetchData(params, officeVisitModes)

        return true
    }

    private fun fetchData(
        params: JobParameters?,
        requestedModes: BooleanArray // office visit modes (rid/cid/vr), 3 items max
    ) {
        val networkService = RetrofitNetworkService(application).getInstance()
        val prefsService = PreferencesService(application)

        val officeIds = prefsService
            .get(application.getString(R.string.selectedOffices))
            .split(",")
            .take(MAX_OFFICES_TO_MONITOR) // hard limit in case of sly users

        // get some PI from preferences
        val code = prefsService.get(application.getString(R.string.recaptchaResponseCode))
        val firstName = prefsService.get(application.getString(R.string.firstName))
        val lastName = prefsService.get(application.getString(R.string.lastName))
        val phone = prefsService.get(application.getString(R.string.phone)).split(".")

        val result = mutableListOf<AvailableDate>()
        val parser = AvailableDatesPageParser()

        GlobalScope.launch {
            val firstOffice = getAvailableDatesForOfficeId(
                networkService,
                parser,
                code,
                firstName,
                lastName,
                phone,
                officeIds[0],
                requestedModes) // make one call first to make sure recaptcha works

            if (firstOffice.dates.any()) {
                result.addAll(firstOffice.dates)

                // load rest (if any)
                repeat(officeIds.count() - 1) {
                    val dates = getAvailableDatesForOfficeId(
                        networkService,
                        parser,
                        code,
                        firstName,
                        lastName,
                        phone,
                        officeIds[it + 1],
                        requestedModes,
                        repeatOnFailure = false) // could not fail since this is not a first call yet

                    result.addAll(dates.dates)
                }
            }

            var resultCode = SUCCESS_MSG
            if (firstOffice.errorCode != 0) {
                resultCode = firstOffice.errorCode
            }

            val uniqueDates = filterOutDuplicateRecords(result)

            notifyActivity(resultCode, uniqueDates, prefsService)
            if (params != null) {
                jobFinished(params, false)
            }
        }
    }

    // first request with a new recaptcha code is always failing for some reason
    // so we need repeatOnFailure parameter for this case
    private suspend fun getAvailableDatesForOfficeId(
        service: IRetrofitNetworkService,
        parser: AvailableDatesPageParser,
        captchaCode: String,
        firstName: String,
        lastName: String,
        phone: List<String>,
        officeId: String,
        modes: BooleanArray,
        repeatOnFailure: Boolean = true): AvailableDates {
        val sessionId = getSessionId(service)
        if (sessionId.isEmpty()) {
            return  AvailableDates(listOf(), "", SERVICE_FAILURE)
        }

        val itemsCount = modes.count { it }
        try {
            val response = service.getDatesForOfficeVisit(
                sessionId,
                "OfficeVisit",
                captchaCode,
                officeId,
                itemsCount,
                firstName,
                lastName,
                phone[0],
                phone[1],
                phone[2],
                "true",
                captchaCode,
                taskRID = modes[0],
                taskCID = modes[1],
                taskVR = modes[2]
            ).await()

            val parsingResult = parser.parseList(response.body() ?: "")

            if (parsingResult.dates.isEmpty() && repeatOnFailure) {
                // possible recaptcha failure, repeat
                return getAvailableDatesForOfficeId(
                    service,
                    parser,
                    captchaCode,
                    firstName,
                    lastName,
                    phone,
                    officeId,
                    modes,
                    repeatOnFailure = false)
            }

            for (date in parsingResult.dates) {
                date.sessionId = sessionId
            }

            parsingResult.sessionId = sessionId

            return  parsingResult
        } catch (e: Exception) {
            // service unavailable
            return  AvailableDates(listOf(), "", SERVICE_FAILURE)
        }
    }
}