package org.ptz.xakep.dmvappointmentsmonitor.parsers

import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDates
import org.ptz.xakep.dmvappointmentsmonitor.services.WRONG_PERSONAL_INFO

class AvailableDatesPageParser {

    fun parseList(input: String): AvailableDates {
        if (input.isEmpty()) return AvailableDates()

        val tag = "td data-title=\"Office\""
        var start = input.indexOf(tag)
        return if (start == -1) {
            val result = AvailableDates()
            // check for WRONG_PI error
            if (input.indexOf("does not match our records") != -1) {
                result.errorCode = WRONG_PERSONAL_INFO
            }

            result
        } else {
            val availableDates = mutableListOf<AvailableDate>()

            var index = 0
            while (true) {
                if (start == -1) break

                start += tag.length + 1
                val end = input.indexOf("</td>", start)
                val office = parseOfficeName(input.substring(start, end).trim())

                start = end + 5 // 5 is </td>
                val appointment = parseAppointment(
                    input.substring(start, input.indexOf("</td>", start)).trim()
                )

                availableDates.add(AvailableDate(
                    office,
                    appointment,
                    availableDates.size != 0,
                    position = index++))

                start = input.indexOf(tag, start)
            }

            AvailableDates(availableDates)
        }
    }

    private fun parseOfficeName(input: String): String {
        return if (input[0] == '<') {
            val s = input.indexOf(">") + 1
            input.substring(s, input.indexOf("<", s)).trim()
        } else {
            input
        }
    }

    private fun parseAppointment(input: String): String {
        val strong = input.indexOf("<strong")
        if (strong == -1) {
            return "Sorry, all appointments at this office are currently taken. Please select another office."
        }

        val e = input.indexOf("</strong", strong)

        return input
            .substring(input.indexOf('>', strong) + 1, e)
            .replace("\t", " ")
            .replace(Regex("[ ]{2,}"), " ") // replace multiple spaces with single one
            .trim()
    }
}