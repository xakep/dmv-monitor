package org.ptz.xakep.dmvappointmentsmonitor

import android.app.Application
import org.ptz.xakep.dmvappointmentsmonitor.components.*
import org.ptz.xakep.dmvappointmentsmonitor.modules.AppModule

class DmvMonitorApplication : Application() {
    lateinit var mainActivityComponent: AppComponent
    lateinit var settingsComponent: SettingsComponent
    lateinit var monitoringComponent: MonitoringComponent
    lateinit var appointmentsComponent: AppointmentsComponent

    override fun onCreate() {
        super.onCreate()

        val app = AppModule(this)

        mainActivityComponent = DaggerAppComponent.builder()
            .appModule(app)
            .build()

        settingsComponent = DaggerSettingsComponent.builder()
            .appModule(app)
            .build()

        monitoringComponent = DaggerMonitoringComponent.builder()
            .appModule(app)
            .build()

        appointmentsComponent = DaggerAppointmentsComponent.builder()
            .appModule(app)
            .build()
    }
}