package org.ptz.xakep.dmvappointmentsmonitor

import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate

interface IMonitoringView {
    fun loadFinished(data: List<AvailableDate>)
    fun serviceStopped() // gets called once booking is finished and service is stopped
    fun onError(code: Int)
}