package org.ptz.xakep.dmvappointmentsmonitor.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import org.ptz.xakep.dmvappointmentsmonitor.R
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.services.Utils
import kotlinx.android.synthetic.main.available_date_card.view.*
import kotlinx.android.synthetic.main.available_date_card.view.officeName

class AvailableDatesAdapter : RecyclerView.Adapter<AvailableDatesAdapter.ViewHolder>() {
    private var items: MutableList<AvailableDate> = mutableListOf()
    private lateinit var cancelBtnOnClickListener: View.OnClickListener

    fun setOnClickListener(listener: View.OnClickListener) {
        cancelBtnOnClickListener = listener
    }

    fun addData(dates: List<AvailableDate>) {
        items.addAll(dates)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun getItemByPosition(pos: Int): AvailableDate? {
        if (pos >= items.size) return null
        return items[pos]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.available_date_card, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        holder.officeNameTxtView.text = data.officeName
        holder.availableDate.text = data.date
        holder.inDays.text = Utils.beautifyInDays(data.inDays)

        holder.makeAppointmentBtn.tag = position
        holder.makeAppointmentBtn.setOnClickListener(cancelBtnOnClickListener)

        holder.makeAppointmentBtn.visibility =
            if (data.inDays != Int.MAX_VALUE) View.VISIBLE else View.GONE
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val officeNameTxtView: TextView = view.officeName
        val availableDate: TextView = view.availableDate
        val inDays: TextView = view.inDays
        val makeAppointmentBtn: Button = view.makeAppointmentBtn
    }
}