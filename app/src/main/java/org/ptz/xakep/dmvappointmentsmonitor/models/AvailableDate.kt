package org.ptz.xakep.dmvappointmentsmonitor.models

import org.ptz.xakep.dmvappointmentsmonitor.services.Utils

data class AvailableDate(
    val officeName: String,
    val date: String,
    // dmv site provides additional office (Info doesn't match the direct request, probably goes from the cache)
    // so, if the same office is selected in settings, ignore this info
    val additionalOffice: Boolean = false,
    val position: Int = 0, // this is position on the page, used to make an appointment
    val inDays: Int = Utils.calcDaysDiff(date),
    var sessionId: String = "")