package org.ptz.xakep.dmvappointmentsmonitor.modules

import org.ptz.xakep.dmvappointmentsmonitor.parsers.AppointmentsPageParser
import org.ptz.xakep.dmvappointmentsmonitor.parsers.OfficesListPageParser
import org.ptz.xakep.dmvappointmentsmonitor.parsers.AvailableDatesPageParser
import dagger.Module
import dagger.Provides

@Module
class ParsersModule {
    @Provides
    fun provideDriveTestPageParser(): OfficesListPageParser = OfficesListPageParser()

    @Provides
    fun provideDriveTestResultsParser(): AvailableDatesPageParser = AvailableDatesPageParser()

    @Provides
    fun provideAppointmentsResultsParser(): AppointmentsPageParser = AppointmentsPageParser()
}