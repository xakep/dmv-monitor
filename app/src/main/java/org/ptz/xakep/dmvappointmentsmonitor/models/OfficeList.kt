package org.ptz.xakep.dmvappointmentsmonitor.models

data class OfficeList(val list: Map<String, Int>, val siteKey: String)