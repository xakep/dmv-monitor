package org.ptz.xakep.dmvappointmentsmonitor.components

import org.ptz.xakep.dmvappointmentsmonitor.MainActivity
import org.ptz.xakep.dmvappointmentsmonitor.modules.AppModule
import org.ptz.xakep.dmvappointmentsmonitor.modules.ServicesModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ServicesModule::class])
interface AppComponent {
    fun inject(target: MainActivity)
}
