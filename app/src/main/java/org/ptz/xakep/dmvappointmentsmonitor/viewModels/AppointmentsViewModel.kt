package org.ptz.xakep.dmvappointmentsmonitor.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.ptz.xakep.dmvappointmentsmonitor.models.Appointment
import org.ptz.xakep.dmvappointmentsmonitor.parsers.AppointmentsPageParser
import org.ptz.xakep.dmvappointmentsmonitor.services.IRetrofitNetworkService
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import org.ptz.xakep.dmvappointmentsmonitor.services.Utils
import kotlinx.coroutines.launch

class AppointmentsViewModel(private val network: RetrofitNetworkService) : ViewModel() {
    companion object {
        val FACTORY = singleArgViewModelFactory(::AppointmentsViewModel)
    }

    private val _errors = MutableLiveData<String>()

    val errors: LiveData<String>
        get() = _errors

    private val _appointments = MutableLiveData<List<Appointment>>()

    val appointments: LiveData<List<Appointment>>
        get() = _appointments

    fun refresh(
        code: String,
        firstName: String,
        lastName: String,
        phone: List<String>
    ) {
        viewModelScope.launch {
            try {
                val id = getSessionId(network.getInstance())
                if (id.isNotEmpty()) {
                    _appointments.value = getAppointments(
                        network.getInstance(),
                        id,
                        code,
                        firstName,
                        lastName,
                        phone
                    )
                }
            } catch (e: Exception) {
                _errors.value = e.message
            }
        }
    }

    fun cancel(
        code: String,
        firstName: String,
        lastName: String,
        phone: List<String>,
        appointmentToCancel: Appointment,
        position: Int // in case of multiple appointments
    ) {
        val service = network.getInstance()
        // 1. get a sessionId (since the previous one could be already expired)
        // 2. get appointments
        // 3. compare: selected appointment should match existing one
        // 4. if match cancel
        viewModelScope.launch {
            try {
                val id = getSessionId(service)
                if (id.isNotEmpty()) {
                    val appts = getAppointments(
                        service,
                        id,
                        code,
                        firstName,
                        lastName,
                        phone,
                        repeatOnFailure = false
                    )

                    if (appts.size <= position || appts[position] !=  appointmentToCancel) {
                        _appointments.value = appts
                    } else {
                        // cancel it
                        val response = service.cancelAppointment(id, position).await()
                        val body = response.body() ?: ""
                        val results = AppointmentsPageParser().parse(body)
                        if (!results.any()) {
                            // All appointments matching this name and phone number have been cancelled.
                            _appointments.value = listOf(
                                Appointment(
                                "All appointments matching this name and phone number have been cancelled.",
                                "",
                                ""))
                        } else {
                            _appointments.value = results
                        }
                    }
                }
            } catch (e: Exception) {
                _errors.value = e.message
            }
        }
    }

    private suspend fun getSessionId(service: IRetrofitNetworkService): String {
        return Utils.extractSessionId(service.getSessionIdAsync().await())
    }

    private suspend fun getAppointments(
        service: IRetrofitNetworkService,
        sessionId: String,
        code: String,
        firstName: String,
        lastName: String,
        phone: List<String>,
        repeatOnFailure: Boolean = true): List<Appointment> {
        val response = service.getMyAppointments(
            sessionId,
            code,
            firstName,
            lastName,
            phone[0],
            phone[1],
            phone[2],
            code
        ).await()

        val it = response.body() ?: ""
        val results = AppointmentsPageParser().parse(it)
        if (results.isEmpty()) {
            // check for recaptcha error and repeat if needed
            if (it.indexOf("Appointment System - Error") != -1 && repeatOnFailure) {
                return getAppointments(
                    network.getInstance(),
                    sessionId,
                    code,
                    firstName,
                    lastName,
                    phone,
                    repeatOnFailure = false
                )
            }
        }

        return results
    }
}