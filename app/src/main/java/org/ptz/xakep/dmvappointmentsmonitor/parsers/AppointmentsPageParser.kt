package org.ptz.xakep.dmvappointmentsmonitor.parsers

import org.ptz.xakep.dmvappointmentsmonitor.models.Appointment
import org.ptz.xakep.dmvappointmentsmonitor.models.ExistingAppointment

class AppointmentsPageParser {
    fun parse(input: String): List<Appointment> {
        val result = mutableListOf<Appointment>()

        val pattern = "<td data-title=\"Appointment\">"
        var startPos = input.indexOf(pattern)
        if (startPos == -1) {
            return result // no appointments found or system unavailable
        }

        var endPos: Int
        while (true) {
            if (startPos == -1) break
            endPos = input.indexOf("</td>", startPos)
            result.add(parseOneAppt(input.substring(startPos, endPos)))

            startPos = input.indexOf(pattern, startPos + pattern.length)
        }

        return result
    }

    fun parseForConflicts(input: String): List<ExistingAppointment> {
        var pattern = "<td data-title=\"You are requesting the following appointment\">"
        var startPos = input.indexOf(pattern)
        if (startPos == -1) {
            return listOf() // no conflicts found
        }

        var endPos: Int
        val result = mutableListOf<ExistingAppointment>()
        for (i in 0 until 2) {
            startPos = input.indexOf(">", startPos + pattern.length) + 1 // find first '>'
            endPos = input.indexOf("<", startPos)
            val officeName = trim(input.substring(startPos, endPos))

            startPos = input.indexOf(">", endPos) + 1
            endPos = input.indexOf("<", startPos)
            val date = trim(input.substring(startPos, endPos))

            result.add(ExistingAppointment(officeName, date, ""))

            if (i == 0) {
                pattern = "<td data-title=\"You have an existing appointment\">"
                startPos = input.indexOf(pattern)
            }
        }

        // first one is requested, second one - existing
        return result
    }

    private fun parseOneAppt(input: String): Appointment {
        var item = input.indexOf("Office")
        var s = input.indexOf(">", item) + 1
        var e = input.indexOf("<", s)

        val officeName = input.substring(s, e).trim()

        item = input.indexOf("Purpose", item)
        s = input.indexOf(">", item) + 1
        e = input.indexOf("<", s)

        val purpose = trim(input.substring(s, e))

        item = input.indexOf("Date", item)
        s = input.indexOf(">", item) + 1
        e = input.indexOf("<", s)

        val date = trim(input.substring(s, e))

        return Appointment(officeName, date, purpose)
    }

    private fun trim(input: String): String {
        return input
            .replace("\t", " ")
            .replace("\n", " ")
            .replace("&nbsp;", " ")
            .replace(Regex("[ ]{2,}"), " ") // replace multiple spaces with single one
            .trim()
    }
}