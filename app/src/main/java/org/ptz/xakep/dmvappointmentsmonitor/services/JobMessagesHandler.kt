package org.ptz.xakep.dmvappointmentsmonitor.services

import android.os.Handler
import android.os.Message
import org.ptz.xakep.dmvappointmentsmonitor.MonitoringActivity
import org.ptz.xakep.dmvappointmentsmonitor.IMonitoringView
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import java.lang.ref.WeakReference

class JobMessagesHandler(activity: MonitoringActivity) : Handler() {

    private val monitoringActivity: WeakReference<IMonitoringView> = WeakReference(activity)

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            SERVICE_FAILURE, WRONG_PERSONAL_INFO -> {
                monitoringActivity.get()?.onError(msg.what)
                return
            }

            BOOKING_COPMLETED -> {
                monitoringActivity.get()?.serviceStopped();
                return
            }

            SUCCESS_MSG -> {
                monitoringActivity.get()?.loadFinished(msg.obj as List<AvailableDate>)
            }
        }
    }
}