package org.ptz.xakep.dmvappointmentsmonitor.components

import org.ptz.xakep.dmvappointmentsmonitor.AppointmentsActivity
import org.ptz.xakep.dmvappointmentsmonitor.modules.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        ParsersModule::class,
        ServicesModule::class]
)
interface AppointmentsComponent {
    fun inject(target: AppointmentsActivity)
}