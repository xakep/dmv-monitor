package org.ptz.xakep.dmvappointmentsmonitor.services

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class CustomConverterFactory : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type?,
        annotations: Array<out Annotation>?,
        retrofit: Retrofit?
    ): Converter<ResponseBody, *> {
        return ResponseBodyConverter.instance()
    }

    internal class ResponseBodyConverter : Converter<ResponseBody, String> {
        companion object {
            fun instance(): ResponseBodyConverter {
                return ResponseBodyConverter()
            }
        }

        override fun convert(responseBody: ResponseBody): String {
            return Utils.inputStreamToString(responseBody.byteStream())
        }
    }
}