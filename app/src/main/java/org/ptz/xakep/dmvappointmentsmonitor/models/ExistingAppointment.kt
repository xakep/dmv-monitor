package org.ptz.xakep.dmvappointmentsmonitor.models

data class ExistingAppointment(val officeName: String, val date: String, var sessionId: String)