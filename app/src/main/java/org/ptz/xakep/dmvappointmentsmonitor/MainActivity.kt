package org.ptz.xakep.dmvappointmentsmonitor

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import org.ptz.xakep.dmvappointmentsmonitor.models.SettingsActivityMode
import org.ptz.xakep.dmvappointmentsmonitor.services.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var prefsService: PreferencesService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val component = (this.application as DmvMonitorApplication).mainActivityComponent
        component.inject(this)

        officeVisitBtn.setOnClickListener {
            if (!isOfficeVisitAvailable()) {
                Toast.makeText(applicationContext, getString(R.string.selectAtLeastOneItem), Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            if (!prefsService.isConfiguredForOfficeVisit()) {
                val intent = Intent(applicationContext, SettingsActivity::class.java)
                intent.putExtra(
                    MESSENGER_REQ_MODES,
                    booleanArrayOf(ridCheckBox.isChecked, cidCheckBox.isChecked, vrCheckBox.isChecked))

                openSettings(intent, SettingsActivityMode.RID)
            } else {
                val intent = Intent(applicationContext, MonitoringActivity::class.java)
                intent.putExtra(
                    MESSENGER_REQ_MODES,
                    booleanArrayOf(ridCheckBox.isChecked, cidCheckBox.isChecked, vrCheckBox.isChecked))

                startActivity(intent)
            }
        }

        driveTestBtn.setOnClickListener {
            if (!prefsService.isConfiguredForDriveTest()) {
                openSettings(Intent(applicationContext, SettingsActivity::class.java), SettingsActivityMode.CID)
            } else {
                startActivity(Intent(applicationContext, MonitoringActivity::class.java))
            }
        }

        viewApptsBtn.setOnClickListener {
            if (!prefsService.isConfiguredForOfficeVisit()) {
                openSettings(Intent(applicationContext, SettingsActivity::class.java), SettingsActivityMode.APPTS)
            } else {
                startActivity(Intent(applicationContext, AppointmentsActivity::class.java))
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel(NOTIF_CHANNEL, "dmvapps channel", NotificationManager.IMPORTANCE_DEFAULT)

            val notificationManager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun openSettings(intent: Intent, settingsActivityMode: SettingsActivityMode) {
        intent.putExtra(getString(R.string.settingsActivityMode), settingsActivityMode)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.add(0, OPEN_SETTING_MENU, 0, getString(R.string.opensettings))
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == OPEN_SETTING_MENU) {
            val intent = Intent(applicationContext, SettingsActivity::class.java)
            intent.putExtra(getString(R.string.settingsActivityMode), SettingsActivityMode.NONE)
            startActivity(intent)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun isOfficeVisitAvailable(): Boolean {
        return ridCheckBox.isChecked || cidCheckBox.isChecked || vrCheckBox.isChecked
    }
}
