package org.ptz.xakep.dmvappointmentsmonitor.services

import android.app.AlertDialog
import android.content.Context
import org.ptz.xakep.dmvappointmentsmonitor.R
import retrofit2.Response
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.util.*

class Utils {
    companion object {
        fun extractSessionId(response: Response<Void>): String {
            val headers = response.headers().values("Set-Cookie")
            for (header in headers) {
                val sessionIdPos = header.indexOf("SESSIONID") // move to strings
                if (sessionIdPos == -1) continue

                val comma = header.indexOf(";") + 1
                return header.substring(0, comma)
            }

            return ""
        }

        fun showConfirmationDialog(ctx: Context, func: () -> Unit) {
            val alert = AlertDialog.Builder(ctx)
            alert.setTitle(ctx.getString(R.string.confirmDialogTitle))

            alert.setPositiveButton(ctx.getString(R.string.confirmDialogBtn)) { dialog, _ ->
                dialog.dismiss()
                func()
            }

            alert.setNegativeButton(ctx.getString(R.string.cancelDialogBtn)) { dialog, _ ->
                dialog.dismiss()
            }

            alert.show()
        }

        fun beautifyInDays(inDays: Int): String {
            if (inDays == Int.MAX_VALUE) return ""
            return when (inDays) {
                -1 -> "Today"
                0 -> "Tomorrow"
                else -> "in " + inDays + " day" + (if (inDays == 1) "" else "s")
            }
        }

        fun inputStreamToString(input: InputStream?) : String {
            if (input == null) return ""
            val buf = StringBuilder()
            val reader = BufferedReader(InputStreamReader(input, "UTF-8") as Reader?)
            var str: String?

            while (true) {
                str = reader.readLine()
                if (str == null) break
                buf.append(str)
            }

            reader.close()

            return buf.toString()
        }

        // date: Sep 30, 2019 at 12:40 PM
        fun calcDaysDiff(date: String): Int {
            if (date.isEmpty() || date.startsWith("Sorry")) {
                return Int.MAX_VALUE
            }

            var pos1 = date.indexOf(" ") // first space
            val month = date.substring(0, pos1) // Sep
            val pos2 = date.indexOf(",", pos1)
            val day = date.substring(pos1 + 1, pos2) // 30
            pos1 = date.indexOf(" ", pos2 + 2) // third space
            val year = date.substring(pos2 + 2, pos1)

            val appointmentDay = Calendar.getInstance()
            appointmentDay.set(year.toInt(), months[month.substring(0, 3)] ?: 1, day.toInt(), 12, 0, 0)

            val today = Calendar.getInstance()
            today.set(Calendar.HOUR, 12)
            today.set(Calendar.MINUTE, 0)
            today.set(Calendar.SECOND, 0)

            return if (appointmentDay.after(today))
                ((appointmentDay.timeInMillis - today.timeInMillis) / (24 * 60 * 60 * 1000)).toInt()
            else -1 // -1 means today
        }

        private var months = mapOf(
            "Jan" to Calendar.JANUARY,
            "Feb" to Calendar.FEBRUARY,
            "Mar" to Calendar.MARCH,
            "Apr" to Calendar.APRIL,
            "May" to Calendar.MAY,
            "Jun" to Calendar.JUNE,
            "Jul" to Calendar.JULY,
            "Aug" to Calendar.AUGUST,
            "Sep" to Calendar.SEPTEMBER,
            "Oct" to Calendar.OCTOBER,
            "Nov" to Calendar.NOVEMBER,
            "Dec" to Calendar.DECEMBER
        )
    }
}