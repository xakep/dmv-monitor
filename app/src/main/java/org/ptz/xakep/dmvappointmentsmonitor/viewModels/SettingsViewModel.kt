package org.ptz.xakep.dmvappointmentsmonitor.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import org.ptz.xakep.dmvappointmentsmonitor.models.Office
import org.ptz.xakep.dmvappointmentsmonitor.parsers.OfficesListPageParser
import org.ptz.xakep.dmvappointmentsmonitor.services.RetrofitNetworkService
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val network: RetrofitNetworkService) : ViewModel() {

    companion object {
        val FACTORY = singleArgViewModelFactory(::SettingsViewModel)
    }

    private val _dataSiteKey = MutableLiveData<String>()

    val dataSiteKey: LiveData<String>
        get() = _dataSiteKey

    private val _offices = MutableLiveData<ArrayList<Office>>()

    val offices: LiveData<ArrayList<Office>>
        get() = _offices

    fun refresh() {
        launchDataLoad {
            network.getInstance().getOfficesAndDataKey()
        }
    }

    private fun launchDataLoad(func: suspend () -> Deferred<Response<String>>): Job {
        return viewModelScope.launch {
            try {
                val response = func().await()
                val data = OfficesListPageParser().parse(response.body() ?: "")

                val list = arrayListOf<Office>()
                for (key in data.list.keys.sorted()) {
                    list.add(Office(key, data.list[key] as Int, false))
                }

                _offices.value = list
                _dataSiteKey.value = data.siteKey
            } catch (e: Exception) {
                // no network
                _offices.value = arrayListOf()
            }
        }
    }
}