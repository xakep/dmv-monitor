package org.ptz.xakep.dmvappointmentsmonitor.services

import org.ptz.xakep.dmvappointmentsmonitor.BuildConfig

@JvmField val NOTIF_CHANNEL = "${BuildConfig.APPLICATION_ID}.DMVAPPS"
@JvmField val MESSENGER_INTENT_KEY = "${BuildConfig.APPLICATION_ID}.MESSENGER_INTENT_KEY"
@JvmField val MESSENGER_REFRESH_NOW = "${BuildConfig.APPLICATION_ID}.MESSENGER_REFRESH_NOW"
@JvmField val MESSENGER_REQ_MODES = "${BuildConfig.APPLICATION_ID}.MESSENGER_REQ_MODES" // requested modes: rid/cid/vr
@JvmField val WRONG_PERSONAL_INFO = 145030

@JvmField val OPEN_SETTING_MENU = 1001
@JvmField val EXPIRED_RECAPTCHA = 102
@JvmField val UNKNOWN_ERROR = 105
@JvmField val SUCCESS_MSG = 451
@JvmField val RECAPTCHA_REQ = 3119
@JvmField val BOOKING_COPMLETED = 784324
@JvmField val SERVICE_FAILURE = 3122

@JvmField val NEW_APP_NOTIF = 452

@JvmField val MAX_OFFICES_TO_MONITOR = 3