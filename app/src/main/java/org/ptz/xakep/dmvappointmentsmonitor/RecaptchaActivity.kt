package org.ptz.xakep.dmvappointmentsmonitor

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import androidx.appcompat.app.AppCompatActivity
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_recaptcha.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class RecaptchaActivity : AppCompatActivity() {

    private val utf8: String = "UTF-8"
    private var dataSiteKey: String = ""

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recaptcha)

        dataSiteKey = intent.getStringExtra(getString(R.string.dataSiteKey))

        webView.settings.javaScriptEnabled = true
        val handler: Handler = object : Handler() {
            override fun handleMessage(mes: Message) {
                webView.loadUrl("javascript:document.getElementById('recaptcha-demo-form').submit()");
            }
        }

        webView.webViewClient = object : WebViewClient() {
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                val url = request.url.toString()
                if (url.endsWith("api2/demo")) {
                    try {
                        return WebResourceResponse("text/html", utf8, getPage())
                    } catch (e: Throwable) {
                        e.printStackTrace()
                    }
                } else if (url.indexOf("userverify") != -1) {
                    // press the Submit button
                    handler.sendMessageDelayed(Message(), 1000)
                }

                return super.shouldInterceptRequest(view, request)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = request?.url.toString()

                val responseTag = getString(R.string.recaptchaResponseTag)
                val pos = url.indexOf(responseTag)
                if (pos != -1) {
                    // check the response code, if empty - try again
                    val code = url.substring(pos + responseTag.length + 1)
                    if (code == "") {
                        return true
                    }

                    val intent = Intent()
                    intent.putExtra(getString(R.string.recaptchaCode), url.substring(pos + responseTag.length + 1))
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                    return true
                }

                return super.shouldOverrideUrlLoading(view, request)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        webView.loadUrl(getString(R.string.recaptchaDemoUrl))
    }

    private fun getPage(): InputStream {
        val pageInputStream = assets.open("page.html")
        val buf = StringBuilder()
        val reader = BufferedReader(InputStreamReader(pageInputStream, utf8))
        var str: String?

        while (true) {
            str = reader.readLine()
            if (str == null) break
            str = str.replace("%DATASITEKEY%", dataSiteKey)
            buf.append(str)
        }

        reader.close()

        return buf.toString().byteInputStream()
    }
}
