package org.ptz.xakep.dmvappointmentsmonitor.models

enum class SettingsActivityMode(val value: Int) {
    NONE(-1),
    APPTS(0),
    RID(1),
    CID(2)
}