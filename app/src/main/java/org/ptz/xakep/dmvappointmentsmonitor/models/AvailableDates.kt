package org.ptz.xakep.dmvappointmentsmonitor.models

data class AvailableDates(
    val dates: List<AvailableDate> = listOf(),
    var sessionId: String = "",
    var errorCode: Int = 0) // 0 means no errors