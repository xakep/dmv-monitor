package org.ptz.xakep.dmvappointmentsmonitor.services

import android.app.Service
import android.app.job.JobParameters
import android.content.Intent
import org.ptz.xakep.dmvappointmentsmonitor.R
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDate
import org.ptz.xakep.dmvappointmentsmonitor.models.AvailableDates
import org.ptz.xakep.dmvappointmentsmonitor.parsers.AvailableDatesPageParser
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DrivingTestMonitoringService: MonitoringServiceBase() {

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        activityMessenger = intent.getParcelableExtra(MESSENGER_INTENT_KEY)

        if (intent.getBooleanExtra(MESSENGER_REFRESH_NOW, false)) {
            fetchData(null)
        } else if (data.size != 0) {
            notifyActivity(SUCCESS_MSG, data, null)
        }

        // service will NOT be restarted in case of system kills it
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters): Boolean {
        fetchData(params)

        return true
    }

    private fun fetchData(params: JobParameters?) {
        val networkService = RetrofitNetworkService(application).getInstance()
        val prefsService = PreferencesService(application)

        val officeIds = prefsService
            .get(application.getString(R.string.selectedOffices))
            .split(",")
            .take(MAX_OFFICES_TO_MONITOR) // hard limit in case of sly users

        // get some PI from preferences
        val code = prefsService.get(application.getString(R.string.recaptchaResponseCode))
        val firstName = prefsService.get(application.getString(R.string.firstName))
        val lastName = prefsService.get(application.getString(R.string.lastName))
        val dlNumber = prefsService.get(application.getString(R.string.dlNumber))
        val dob = prefsService.get(application.getString(R.string.birthDate)).split("/")
        val phone = prefsService.get(application.getString(R.string.phone)).split(".")

        val result = mutableListOf<AvailableDate>()
        val parser = AvailableDatesPageParser()

        GlobalScope.launch {
            val firstOffice = getAvailableDatesForDrivingTest(
                networkService,
                parser,
                code,
                dob,
                dlNumber,
                firstName,
                lastName,
                phone,
                officeIds[0]) // make one call first to make sure recaptcha works

            if (firstOffice.dates.any()) {
                result.addAll(firstOffice.dates)

                // load rest (if any)
                repeat(officeIds.count() - 1) {
                    val dates = getAvailableDatesForDrivingTest(
                        networkService,
                        parser,
                        code,
                        dob,
                        dlNumber,
                        firstName,
                        lastName,
                        phone,
                        officeIds[it + 1],
                        repeatOnFailure = false) // could not fail since this is not a first call yet

                    result.addAll(dates.dates)
                }
            }

            var resultCode = SUCCESS_MSG
            if (firstOffice.errorCode != 0) {
                resultCode = firstOffice.errorCode
            }

            // will be empty in case of errors
            val uniqueDates = filterOutDuplicateRecords(result)

            notifyActivity(resultCode, uniqueDates, prefsService)
            if (params != null) {
                jobFinished(params, false)
            }
        }
    }

    // first request with a new recaptcha code is always failing for some reason
    // so we need repeatOnFailure parameter for this case
    private suspend fun getAvailableDatesForDrivingTest(
        service: IRetrofitNetworkService,
        parser: AvailableDatesPageParser,
        captchaCode: String,
        dob: List<String>,
        dlNumber: String,
        firstName: String,
        lastName: String,
        phone: List<String>,
        officeId: String,
        repeatOnFailure: Boolean = true): AvailableDates {
        val sessionId = getSessionId(service)
        if (sessionId.isEmpty()) {
            return  AvailableDates(listOf(), "", SERVICE_FAILURE)
        }

        try {
            val response = service.getDatesForDrivingTest(
                sessionId,
                1,
                "DriveTest",
                captchaCode,
                officeId,
                "DT",
                firstName,
                lastName,
                dlNumber,
                dob[0],
                dob[1],
                dob[2],
                phone[0],
                phone[1],
                phone[2],
                "true",
                captchaCode
            ).await()

            val parsingResult = parser.parseList(response.body() ?: "")

            if (parsingResult.errorCode == WRONG_PERSONAL_INFO) {
                // display an error
                return AvailableDates(errorCode = WRONG_PERSONAL_INFO)
            }

            if (parsingResult.dates.isEmpty() && repeatOnFailure) {
                // possible recaptcha failure, repeat
                return getAvailableDatesForDrivingTest(
                    service,
                    parser,
                    captchaCode,
                    dob,
                    dlNumber,
                    firstName,
                    lastName,
                    phone,
                    officeId,
                    repeatOnFailure = false
                )
            }

            for (date in parsingResult.dates) {
                date.sessionId = sessionId
            }

            parsingResult.sessionId = sessionId

            return parsingResult
        } catch (e: Exception) {
            return  AvailableDates(listOf(), "", SERVICE_FAILURE)
        }
    }
}