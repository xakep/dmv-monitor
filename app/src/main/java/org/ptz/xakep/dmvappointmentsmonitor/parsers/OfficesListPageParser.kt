package org.ptz.xakep.dmvappointmentsmonitor.parsers

import org.ptz.xakep.dmvappointmentsmonitor.models.OfficeList
import java.lang.Integer.parseInt

class OfficesListPageParser {
    fun parse(input: String): OfficeList {
        var start = input.indexOf("id=\"officeId\"")
        if (start == -1) {
            return OfficeList(mapOf(), "") // service unavailable or other errors
        } else {
            // 1. parse offices
            var end = input.indexOf("</select>", start)
            val offices = input.substring(start, end)
            val map = HashMap<String, Int>()
            for (office in offices.split("/")) {
                start = office.indexOf("value=")
                if (start == -1) continue
                start += 7 // len of 'value="'
                end = office.indexOf("\"", start)
                val id = office.substring(start, end)
                start = end + 2
                end = office.indexOf("<", start)
                val name = office.substring(start, end)
                map[name] = parseInt(id)
            }

            // 2. parse data-sitekey
            start = input.indexOf("data-sitekey") + 14 // 14 is len of "data-sitekey='"
            end = input.indexOf("'", start)
            val key = input.substring(start, end)

            return OfficeList(map, key)
        }
    }
}