package org.ptz.xakep.dmvappointmentsmonitor.components

import org.ptz.xakep.dmvappointmentsmonitor.MonitoringActivity
import org.ptz.xakep.dmvappointmentsmonitor.modules.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        ServicesModule::class,
        ParsersModule::class]
)
interface MonitoringComponent {
    fun inject(target: MonitoringActivity)
}